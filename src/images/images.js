export const PLAYER_UNIT_IMAGES = [
    {
        hoplite: require('../images/units/redhoplite.png'),
        settler: require('../images/units/redsettler.png'),
        caravan: require('../images/units/redcaravan.png'),
        mercenary: require('../images/units/redmercenary.png')
    },
    {
        hoplite: require('../images/units/bluehoplite.png'),
        settler: require('../images/units/bluesettler.png'),
        caravan: require('../images/units/bluecaravan.png'),
        mercenary: require('../images/units/bluemercenary.png')
    },
    {
        hoplite: require('../images/units/whitehoplite.png'),
        settler: require('../images/units/whitesettler.png'),
        caravan: require('../images/units/whitecaravan.png'),
        mercenary: require('../images/units/whitemercenary.png')
    },
    {
        hoplite: require('../images/units/yellowhoplite.png'),
        settler: require('../images/units/yellowsettler.png'),
        caravan: require('../images/units/yellowcaravan.png'),
        mercenary: require('../images/units/yellowmercenary.png')
    },
];

export const CARD_IMAGES = {
    "Call to arms": require("../images/cards/callToArms.jpg"),
    "Camel whisperer": require("../images/cards/camelWhisperer.jpg"),
    "Convert barbarian": require("../images/cards/convertBarbarian.jpg"),
    "Fortification expert": require("../images/cards/fortificationExpert.jpg"),
    "General": require("../images/cards/general.jpg"), 
    "Holy war": require("../images/cards/holyWar.jpg"),
    "Mercenary": require("../images/cards/mercenary.jpg"),
    "Mutiny": require("../images/cards/mutiny.jpg"),
    "Nationalism": require("../images/cards/nationalism.jpg"),
    "Pacifism": require("../images/cards/pacifism.jpg"),
    "Propaganda": require("../images/cards/propaganda.jpg"),
    "Siege expert": require("../images/cards/siegeExpert.jpg"),
    "Siege weapon engineer": require("../images/cards/siegeWeaponEngineer.jpg")
};

export const TILE_IMAGES = {
    "D": require("../images/terrains/desert.png"),
    "F": require("../images/terrains/forrest.png"),
    "V": require("../images/terrains/hut.png"),
    "M": require("../images/terrains/mountains.png"),
    "P": require("../images/terrains/plains.png"), 
    "R": require("../images/terrains/river.png")
};