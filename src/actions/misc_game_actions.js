const ACTION_TYPE = "game";

export const types = {
  SET_GAME_STATE: 'set_game_state',
  END_TURN: 'end_turn',
  REFRESH: 'refresh'
};

export function setState(state, {gameId}) {
  return {
    category: ACTION_TYPE,
    type: types.SET_GAME_STATE,
    state,
    gameId
  };
}

export function endTurn({gameId}) {
  return {
    category: ACTION_TYPE,
    type: types.END_TURN,
    gameId,
    meta: {
        remote: true   
    }
  };
}

export function refresh({gameId}) {
  return {
    category: ACTION_TYPE,
    type: types.REFRESH,
    gameId
  };
}
