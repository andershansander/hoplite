const ACTION_TYPE = "image";

export const types = {
  LOAD_UNIT_IMAGE: 'load_unit_image',
  LOAD_CARD_IMAGE: 'load_card_image',
  LOAD_TILE_IMAGE: 'load_tile_image',
  UNIT_IMAGE_LOADED: 'unit_image_loaded',
  CARD_IMAGE_LOADED: 'card_image_loaded',
  TILE_IMAGE_LOADED: 'tile_image_loaded'
};

export function loadCardImage({cardId}) {
  return {
    category: ACTION_TYPE,
    type: types.LOAD_CARD_IMAGE,
    cardId
  };
}

export function loadUnitImage({playerIndex, unit}) {
  return {
    category: ACTION_TYPE,
    type: types.LOAD_UNIT_IMAGE,
    playerIndex,
    unit
  };
}

export function unitImageLoaded({playerIndex, unit, image}) {
  return {
    category: ACTION_TYPE,
    type: types.UNIT_IMAGE_LOADED,
    playerIndex: playerIndex,
    unit,
    image
  };
}

export function cardImageLoaded({cardId, image}) {
  return {
    category: ACTION_TYPE,
    type: types.CARD_IMAGE_LOADED,
    cardId,
    image
  };
}

export function loadTileImage({tileTypeKey}) {
  return {
    category: ACTION_TYPE,
    type: types.LOAD_TILE_IMAGE,
    tileTypeKey
  };
}

export function tileImageLoaded({tileTypeKey, image}) {
  return {
    category: ACTION_TYPE,
    type: types.TILE_IMAGE_LOADED,
    tileTypeKey,
    image
  };
}
