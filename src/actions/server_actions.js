const ACTION_TYPE = "server";

export const types = {
  SET_STATE: 'set_state'
};

export function setState(newState) {
  return {
    category: ACTION_TYPE,
    type: types.SET_STATE,    
    state: newState
  };
}
