const ACTION_TYPE = "auth";

export const types = {
  LOGIN: 'login',
  LOGIN_SUCCEEDED: 'login_succeeded',
  LOGIN_FAILED: 'login_failed',
  TRY_AUTO_LOGIN: 'auto_login',
  SOCKET_DISCONNECTED: 'socket_disconnected',
  SOCKET_CONNECTED: 'socket_connected'
};

export function login({username, password}) {
  return {
    category: ACTION_TYPE,
    type: types.LOGIN,
    username,
    password
  }
}

export function loginSucceeded({token, validUntil, username}) {
  return {
    category: ACTION_TYPE,
    type: types.LOGIN_SUCCEEDED,
    token,
    validUntil,
    username
  }
}

export function loginFailed({message}) {
  return {
    category: ACTION_TYPE,
    type: types.LOGIN_FAILED,
    message
  }
}

export function tryAutoLogin() {
  return {
    category: ACTION_TYPE,
    type: types.TRY_AUTO_LOGIN
  };
}

export function socketConnected(socket) {
  return {
    category: ACTION_TYPE,
    type: types.SOCKET_CONNECTED,
    socket: socket
  };
}

export function socketDisconnected(reason) {
  return {
    category: ACTION_TYPE,
    type: types.SOCKET_DISCONNECTED,
    reason: reason
  }
}