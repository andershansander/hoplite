const ACTION_TYPE = "game";

export const types = {
  BID: 'bid_on_card',
  PASS: 'pass_on_card'
};

export function bid({gold, gameId}) {
  return {
    category: ACTION_TYPE,
    type: types.BID,
    gold,
    gameId,
    meta: {
        remote: true
    }
  };
}

export function pass({gameId}) {
  return {
    category: ACTION_TYPE,
    type: types.PASS,
    gameId,
    meta: {
        remote: true
    }
  };
}
