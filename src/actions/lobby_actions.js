const ACTION_TYPE = "lobby";

export const types = {
  SET_LOBBY_STATE: 'set_lobby_state',
  SHOW_LOBBY_TAB: 'show_lobby_tab',
  SHOW_GAME_TAB: 'show_game_tab',
  JOIN_GAME: 'join_game',
  CREATE_GAME: 'create_game'
};

export function setState(newState) {
  return {
    category: ACTION_TYPE,
    type: types.SET_LOBBY_STATE,
    state: newState
  }
}

export function showLobbyTab() {
  return {
    category: ACTION_TYPE,
    type: types.SHOW_LOBBY_TAB
  }
}

export function showGameTab({gameId}) {
  return {
    category: ACTION_TYPE,
    type: types.SHOW_GAME_TAB,
    gameId
  }
}

export function joinGame({gameId}) {
  return {
    category: ACTION_TYPE,
    type: types.JOIN_GAME,
    gameId
  }
}

export function createGame({maxPlayers}) {
  return {
    category: ACTION_TYPE,
    type: types.CREATE_GAME,
    maxPlayers
  }
}