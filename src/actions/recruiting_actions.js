const ACTION_TYPE = "game";

export const types = {
  RECRUIT_CARAVAN: 'recruit_caravan',
  RECRUIT_HOPLITE: 'recruit_hoplite',
  RECRUIT_SETTLER: 'recruit_settler'
};

export function recruitCaravan({target, gameId}) {
  return {
    category: ACTION_TYPE,
    type: types.RECRUIT_CARAVAN,
    target: target,
    gameId,
    meta: {
        remote: true
    }
  };
}

export function recruitHoplite({target, gameId}) {
  return {
    category: ACTION_TYPE,
    type: types.RECRUIT_HOPLITE,
    target: target,
    gameId,
    meta: {
        remote: true
    }
  };
}

export function recruitSettler({target, gameId}) {
  return {
    category: ACTION_TYPE,
    type: types.RECRUIT_SETTLER,
    target: target,
    gameId,
    meta: {
        remote: true
    }
  };
}