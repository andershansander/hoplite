const ACTION_TYPE = "game";

export const types = {
  MOVE_SETTLER: 'move_settler',
  MOVE_HOPLITE: 'move_hoplite',
  BUILD_CITY: 'build_city',
  MARK_FOR_REMOVAL: 'mark_for_removal',
  CONFIRM_HOPLITE_REMOVAL: 'confirm_hoplite_removal',
  REMOVE_HOPLITES: 'remove_hoplites'
};

export function moveSettler({settlerIndex, target, gameId}) {
  return {
    category: ACTION_TYPE,
    type: types.MOVE_SETTLER,
    target: target,
    settlerIndex: settlerIndex,
    gameId,
    meta: {
        remote: true
    }
  };
}

export function moveHoplite({hopliteIndex, target, gameId}) {
  return {
    category: ACTION_TYPE,
    type: types.MOVE_HOPLITE,
    target: target,
    hopliteIndex: hopliteIndex,
    gameId,
    meta: {
        remote: true
    }
  };
}


export function buildCity({settlerIndex, gameId}) {
  return {
    category: ACTION_TYPE,
    type: types.BUILD_CITY,
    settlerIndex: settlerIndex,
    gameId,  
    meta: {
        remote: true
    }
  };
}


export function markHopliteForRemoval({hopliteIndex, gameId}) {
  return {
    category: ACTION_TYPE,
    type: types.MARK_FOR_REMOVAL,
    hopliteIndex: hopliteIndex,
    gameId
  };
}

export function confirmHopliteRemoval({gameId}) {
  return {
    category: ACTION_TYPE,
    type: types.CONFIRM_HOPLITE_REMOVAL,
    gameId
  };
}

export function removeHoplites({hopliteIndices, gameId}) {
  return {
    category: ACTION_TYPE,
    type: types.REMOVE_HOPLITES,
    hopliteIndices: hopliteIndices,
    gameId,
    meta: {
        remote: true
    }
  };
}