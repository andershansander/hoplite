import React from 'react';
import shallowCompare from 'react-addons-shallow-compare';
import {connect} from 'react-redux';
import * as Konva from 'react-konva';
import {MAP_WIDTH, MAP_RIGHT_MARGIN} from './util/canvas_util.js';
import * as actionCreators from '../actions/unit_actions.js';
import * as customActionCreators from '../actions/misc_game_actions.js';
import CardBidding from './CardBidding.jsx';

const phaseNames = [
    "Move settlers/build cities",
    "Move hoplites, first step",
    "Solve conflicts",
    "Move hoplites, second step",
    "Solve conflicts",
    "Deal cards",
    "Update card counters",
    "Play cards",
    "Recruit",
    "Income",
    "Pay mercenaries"
];

const InfoBox = React.createClass({
    shouldComponentUpdate: function(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState);
    },

    endTurn: function () {
        if (this.isSolveConflictPhase()) {
            this.props.confirmHopliteRemoval({gameId: this.props.currentGameId});
        } else {
            this.props.endTurn({gameId: this.props.currentGameId});
        }        
    },
    
    getCurrentPlayer: function () {
        return this.isSolveConflictPhase() ? this.props.solvis : this.props.currentPlayer;
    },

    getCurrentPlayerText: function () {
        return <Konva.Text x={MAP_WIDTH + MAP_RIGHT_MARGIN} y={16} text={"Current player: " + this.props.players.get(this.getCurrentPlayer()).get('name')} fontSize={16} fill="white" />;
    },

    getCurrentPhaseText: function () {
        return <Konva.Text x={MAP_WIDTH + MAP_RIGHT_MARGIN} y={36} text={"Current phase: " + phaseNames[this.props.phase - 1]} fontSize={16} fill="white" />;
    },

    getCurrentPlayerResources: function () {
        const player = this.props.players.get(this.getCurrentPlayer());
        return <Konva.Group>
            <Konva.Text x={MAP_WIDTH + MAP_RIGHT_MARGIN} y={76} text={"Food: " + player.get('food')} fontSize={16} fill="white" />
            <Konva.Text x={MAP_WIDTH + MAP_RIGHT_MARGIN} y={96} text={"Gold: " + player.get('gold')} fontSize={16} fill="white" />
            <Konva.Text x={MAP_WIDTH + MAP_RIGHT_MARGIN} y={116} text={"Industry: " + player.get('industry')} fontSize={16} fill="white" />
        </Konva.Group>
    },

    waitingForOtherPlayerText: function () {
        return <Konva.Text x={MAP_WIDTH + MAP_RIGHT_MARGIN} y={156} text={"Waiting for other player..."} fontSize={16} fill="white" />;
    },

    endTurnButton: function () {
        if (this.props.phase === this.props.phases.get('DEAL_CARD')) {
            return null;
        } else {
            return <Konva.Text x={MAP_WIDTH + MAP_RIGHT_MARGIN} y={156} text={"Click to end turn"} fontSize={24} fill="white" onClick={this.endTurn} />;
        }        
    },

    isSolveConflictPhase: function () {
        return this.props.phase === this.props.phases.get('SOLVE_CONFLICTS_1') || this.props.phase === this.props.phases.get('SOLVE_CONFLICTS_2');
    },

    render: function () {
        const isCurrentPlayer = (!this.isSolveConflictPhase() && this.props.username === this.props.players.get(this.props.currentPlayer).get('name')) ||
            (this.isSolveConflictPhase() && this.props.players.get(this.props.solvis) && this.props.username === this.props.players.get(this.props.solvis).get('name'));
        
        return <Konva.Group>
            {this.getCurrentPlayerText()}
            {this.getCurrentPhaseText()}
            {this.getCurrentPlayerResources()}
            {isCurrentPlayer && this.props.phase === this.props.phases.get('DEAL_CARD') ? <CardBidding /> : null}
            {isCurrentPlayer ? this.endTurnButton() : this.waitingForOtherPlayerText()}
        </Konva.Group>
    }
});

function mapStateToProps(state) {    
    const currentGameId = state.getIn(['lobbyState', 'tab', 'data']).toString();
    return {
        players: state.getIn(['gameState', currentGameId, 'players']),
        phase: state.getIn(['gameState', currentGameId, 'phase']),
        phases: state.getIn(['gameState', currentGameId, 'phases']),
        currentPlayer: state.getIn(['gameState', currentGameId, 'currentPlayer']),
        solvis: state.getIn(['gameState', currentGameId, 'solvis']),
        images: state.getIn(['imageState', 'images']),
        currentGameId: currentGameId,
        username: state.getIn(['authState', 'username'])
    };
}

export default connect(mapStateToProps, Object.assign({}, customActionCreators, actionCreators), null, { pure: false })(InfoBox);