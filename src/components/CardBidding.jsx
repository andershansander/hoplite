import React from 'react';
import shallowCompare from 'react-addons-shallow-compare';
import * as actionCreators from '../actions/card_bidding_actions.js';
import {connect} from 'react-redux';
import * as Konva from 'react-konva';
import {MAP_WIDTH, MAP_RIGHT_MARGIN, INFORMATION_BOX_HEIGHT} from './util/canvas_util.js';

const Recruit = React.createClass({
    shouldComponentUpdate: function(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState);
    },

    cardImageExist: function () {
        return this.props.images && this.props.cardId && this.props.images.get('cards') && this.props.images.get('cards').get(this.props.cardId);
    },

    bidOnCard: function () {
        let bid = this.props.cardBid ? this.props.cardBid.get('gold') + 1 : 3;
        this.props.bid({gold: bid, gameId: this.props.currentGameId});        
    },
    passOnCard: function () {
        this.props.pass({gameId: this.props.currentGameId});        
    },

    getPassButton: function () {
        return <Konva.Text x={MAP_WIDTH + MAP_RIGHT_MARGIN} y={156} text={"Pass on card"} fontSize={16} fill="white" onClick={this.passOnCard} />;
    },
    getBidButton: function () {
        return <Konva.Text x={MAP_WIDTH + MAP_RIGHT_MARGIN + 100} y={156} text={"Bid on card"} fontSize={16} fill="white" onClick={this.bidOnCard} />;
    },

    render: function () {
        return <Konva.Group>
            {this.cardImageExist() ? <Konva.Rect x={MAP_WIDTH} y={INFORMATION_BOX_HEIGHT} draggable={false} opacity={1} width={380} height={380} fillPatternScale={{x: 0.338, y: 0.338}} fillPatternImage={this.props.images.get('cards').get(this.props.cardId)} /> : null}
            {this.getPassButton()}
            {this.getBidButton()}
            <Konva.Text x={MAP_WIDTH - 100} y={156} text={"Current bid " + (this.props.cardBid ? this.props.cardBid.get('gold') : "")} fontSize={16} fill="white" />;
            
        </Konva.Group>
    }
});

function mapStateToProps(state) {
    const currentGameId = state.getIn(['lobbyState', 'tab', 'data']).toString();
    return {
        images: state.getIn(['imageState', 'images']),
        players: state.getIn(['gameState', currentGameId, 'players']),
        currentPlayer: state.getIn(['gameState', currentGameId, 'currentPlayer']),
        cardId: state.getIn(['gameState', currentGameId, 'shownCard', 'id']),
        cardBid: state.getIn(['gameState', currentGameId, 'cardBid']),
        currentGameId: currentGameId
    };
}

export default connect(mapStateToProps, actionCreators, null, { pure: false })(Recruit);