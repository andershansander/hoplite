import React from 'react';
import shallowCompare from 'react-addons-shallow-compare';
import * as actionCreators from '../actions/unit_actions.js';
import * as customActionCreators from '../actions/misc_game_actions.js';
import {connect} from 'react-redux';
import * as Konva from 'react-konva';
import Tiles from './Tiles.jsx';
import Recruit from './Recruit.jsx';
import BuildCity from './BuildCity.jsx';
import {toCanvasCoodinates, HEX_RADIUS} from './util/canvas_util.js';
import {List} from 'immutable';

let cityKey = 0;
let unitKey = 0;
let caravanKey = 0;

const playerColors = [
    "rgb(237,28,36)",
    "rgb(0,0,255)",


];
const Units = React.createClass({
    shouldComponentUpdate: function(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState);
    },

    getCities: function () {
        let cities = [];
        let caravans = [];
        if (this.props.players) {
            this.props.players.forEach((player, playerIndex) => {
                if (player.get('cities')) {
                    const playerCities = player.get('cities').map((city, cityIndex) => {
                        const cityLocation = toCanvasCoodinates({ x: city.get('x'), y: city.get('y') });
                        return <Konva.Rect key={cityKey++} x={cityLocation.x - 10} y={cityLocation.y - 10} width={20} height={20} fill={playerColors[playerIndex]} stroke={playerColors[playerIndex]} strokeWidth={1} />;
                    });
                    const playerCaravans = player.get('cities').filter(city => city.get('hasCaravan') && this.imageExist(playerIndex, 'caravan')).map((city, cityIndex) => {
                        const cityLocation = toCanvasCoodinates({ x: city.get('x'), y: city.get('y') });
                        return <Konva.Circle key={caravanKey++} x={cityLocation.x - 25} y={cityLocation.y + 15} radius={20} fillPatternOffset={{x: -160, y: -150}} fillPatternScale={{x: 0.2, y: 0.2}} fillPatternImage={this.props.images.get(playerIndex).get('caravan')} />;
                    });
                    caravans = caravans.concat(playerCaravans.toJS());
                    cities = cities.concat(playerCities.toJS());
                }
            });
        }
        return <Konva.Group>
            {cities}
            {caravans}
        </Konva.Group>;
    },

    imageExist: function (playerIndex, unit) {
        return this.props.images && this.props.images.get(playerIndex) && this.props.images.get(playerIndex).get(unit);
    },

    isSolveConflictPhase: function () {
        return this.props.phase === this.props.phases.get('SOLVE_CONFLICTS_1') || this.props.phase === this.props.phases.get('SOLVE_CONFLICTS_2');
    },

    isHoplite: function (unitType) {
        return unitType === 'hoplites' || unitType === 'mercenaries';
    },

    unitDropped: function ({unitType, unitIndex, startSquare}, event) {
        var pos = this.props.getStage().getPointerPosition();
        var shape = this.props.getTilesLayer().getIntersection(pos);
        if (!this.isSolveConflictPhase()) {
            if (!shape) {
                this.props.refresh({gameId: this.props.currentGameId});
                return;
            }            
            if (unitType === 'settlers') {
                this.props.moveSettler({settlerIndex: unitIndex, target: shape.attrs.data, gameId: this.props.currentGameId});
            } else if (this.isHoplite(unitType)) {
                this.props.moveHoplite({hopliteIndex: unitIndex, target: shape.attrs.data, gameId: this.props.currentGameId});
            }
        } 
    },

    isClickable: function () {
        return this.isSolveConflictPhase();
    },
    isDraggable: function (ownerPlayerIndex) {
        const isDraggable = ownerPlayerIndex === this.props.currentPlayer && ( 
            this.props.phase === this.props.phases.get('MOVE_SETTLERS') ||
            this.props.phase === this.props.phases.get('MOVE_HOPLITES_1') ||
            this.props.phase === this.props.phases.get('MOVE_HOPLITES_2'));
        return isDraggable;
    },

    unitClicked: function ({unitType, unitIndex}, event) {
        if (this.isSolveConflictPhase() && this.isHoplite(unitType)) {
            this.props.markHopliteForRemoval({hopliteIndex: unitIndex, gameId: this.props.currentGameId});
        }
    },

    getUnits: function (unitType, imageName) {
        const unitTypeOffset = {
            settlers: -19,
            hoplites: 19
        }
        let units = List();        
        if (this.props.players) {
            this.props.players.forEach((player, playerIndex) => {               
                if (player.get(unitType) && this.imageExist(playerIndex, imageName)) {
                    units = units.concat(player.get(unitType).map((unit, unitIndex) => {
                        const unitLocation = toCanvasCoodinates({ x: unit.get('x'), y: unit.get('y') });
                        const amount = player.get(unitType).filter(eachUnit => eachUnit.get('x') === unit.get('x') && eachUnit.get('y') === unit.get('y') && !eachUnit.get('markedForRemoval')).size;
                        const amountLabel = amount > 1 ? <Konva.Text x={unitLocation.x - 2} y={unitLocation.y + 6} text={amount} fontSize={20} fill="black" /> : null;  
                        if (unit.get('markedForRemoval')) {
                            return null;
                        }
                        return {
                            unitIcon: (<Konva.Group key={unitKey++}>                              
                                        <Konva.Circle key={unitKey++} onClick={this.isClickable() ? this.unitClicked.bind(this, {unitType, unitIndex}) : undefined} onDragEnd={this.unitDropped.bind(this, {unitType: unitType, unitIndex: unitIndex, startSquare: {x: unit.get('x'), y: unit.get('y')}})} draggable={this.isDraggable(playerIndex)} x={unitLocation.x + unitTypeOffset[unitType]} y={unitLocation.y + unitTypeOffset[unitType]} radius={20} fillPatternOffset={{x: -160, y: -150}} fillPatternScale={{x: 0.2, y: 0.2}} fillPatternImage={this.props.images.get(playerIndex).get(imageName)} />
                                        {amountLabel}
                                    </Konva.Group>),
                            hasMoved: unit.get('hasMoved')
                        };
                    }));
                }
            });
        }
        units = units.filter(unit => unit !== null);
        return units.sort(unit1 => unit1.hasMoved ? -1 : 1).map(unit => unit.unitIcon).toJS();
    },

    render: function () {
        var tiles = null;
        return (
            <Konva.Group>             
                {this.getCities() }
                {this.getUnits('settlers', 'settler')}
                {this.getUnits('hoplites', 'hoplite')}
            </Konva.Group>
        );
    }
});

function mapStateToProps(state) {
    const currentGameId = state.getIn(['lobbyState', 'tab', 'data']).toString();
    return {
        players: state.getIn(['gameState', currentGameId, 'players']),
        currentPlayer: state.getIn(['gameState', currentGameId, 'currentPlayer']),
        phase: state.getIn(['gameState', currentGameId, 'phase']),
        phases: state.getIn(['gameState', currentGameId, 'phases']),
        images: state.getIn(['imageState', 'images']),
        refreshId: state.getIn(['gameState', currentGameId, 'refreshId']),
        currentGameId: currentGameId
    };
}

export default connect(mapStateToProps, Object.assign({}, actionCreators, customActionCreators), null, { pure: false })(Units);