import React from 'react';
import shallowCompare from 'react-addons-shallow-compare';
import * as actionCreators from '../actions/recruiting_actions.js';
import * as customActionCreators from '../actions/misc_game_actions.js';
import {connect} from 'react-redux';
import * as Konva from 'react-konva';
import {MAP_WIDTH, MAP_RIGHT_MARGIN, INFORMATION_BOX_HEIGHT} from './util/canvas_util.js';

const unitCosts = {
    settler: {food: 4, gold: 2},
    hoplite: {food: 1, gold: 2, industry: 1},
    caravan: {food: 2, gold: 2}
};

const Recruit = React.createClass({
    shouldComponentUpdate: function(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState);
    },

    imageExist: function (playerIndex, unit) {
        return this.props.images && this.props.images.get(playerIndex) && this.props.images.get(playerIndex).get(unit);
    },

    recruitUnit: function (unitType, unitIndex) {
        var pos = this.props.getStage().getPointerPosition();
        var shape = this.props.getTilesLayer().getIntersection(pos);
        if (!shape) {
            this.props.refresh({gameId: this.props.currentGameId});
            return;
        }
        const ownCityAtTarget = this.props.players.get(this.props.currentPlayer).get('cities').find(city => city.get('x') === shape.attrs.data.x && city.get('y') === shape.attrs.data.y); 
        if (unitType === 'settler' && ownCityAtTarget) {
            this.props.recruitSettler({target: shape.attrs.data, gameId: this.props.currentGameId});
        } else if (unitType === 'hoplite' && ownCityAtTarget) {
            this.props.recruitHoplite({target: shape.attrs.data, gameId: this.props.currentGameId});
        } else if (unitType === 'caravan' && ownCityAtTarget && !ownCityAtTarget.get('hasCaravan')) {
            this.props.recruitCaravan({target: shape.attrs.data, gameId: this.props.currentGameId});
        }
    },

    canAfford: function (player, unitType) {
        const playerRegularHoplites = player.get('hoplites').filter(hoplite => !hoplite.isMercenary);
        return player.get('gold') >= unitCosts[unitType].gold && player.get('food') >= unitCosts[unitType].food && (!unitCosts[unitType].industry || player.get('industry') > playerRegularHoplites.size);
    },

    getRecruitButton: function (unitType, {x, y}) {
        if (!this.imageExist(this.props.currentPlayer, unitType)) {
            return null;
        }
        const player = this.props.players.get(this.props.currentPlayer);
        if (!this.canAfford(player, unitType)) {
            return <Konva.Circle x={x} y={y} opacity={0.5} radius={40} fillPatternOffset={{x: -160, y: -150}} draggable={false} fillPatternScale={{x: 0.35, y: 0.35}} fillPatternImage={this.props.images.get(this.props.currentPlayer).get(unitType)} />
        }        
        return <Konva.Circle opacity={1} onDragEnd={this.recruitUnit.bind(this, unitType)} draggable={true} x={x} y={y} radius={40} fillPatternOffset={{x: -160, y: -150}} fillPatternScale={{x: 0.35, y: 0.35}} fillPatternImage={this.props.images.get(this.props.currentPlayer).get(unitType)} />
    },

    render: function () {
        return <Konva.Group>
            <Konva.Text x={MAP_WIDTH + MAP_RIGHT_MARGIN} y={INFORMATION_BOX_HEIGHT + 16} text="Recruiting" fontSize={30} fill="white" />
            {this.getRecruitButton('settler', {x: MAP_WIDTH + MAP_RIGHT_MARGIN, y: INFORMATION_BOX_HEIGHT + 116})}
            {this.getRecruitButton('caravan', {x: MAP_WIDTH + MAP_RIGHT_MARGIN, y: INFORMATION_BOX_HEIGHT + 216})}
            {this.getRecruitButton('hoplite', {x: MAP_WIDTH + MAP_RIGHT_MARGIN, y: INFORMATION_BOX_HEIGHT + 316})}
        </Konva.Group>
    }
});

function mapStateToProps(state) {
    const currentGameId = state.getIn(['lobbyState', 'tab', 'data']).toString();
    return {
        images: state.getIn(['imageState', 'images']),
        players: state.getIn(['gameState', currentGameId, 'players']),
        currentPlayer: state.getIn(['gameState', currentGameId, 'currentPlayer']),
        refreshId: state.getIn(['gameState', currentGameId, 'refreshId']),
        currentGameId: currentGameId
    };
}

export default connect(mapStateToProps, Object.assign({}, actionCreators, customActionCreators), null, { pure: false })(Recruit);