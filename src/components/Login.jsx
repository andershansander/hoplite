import React from 'react';
import shallowCompare from 'react-addons-shallow-compare';
import {connect} from 'react-redux';
import * as authActions from '../actions/auth_actions.js';

const Login = React.createClass({
    shouldComponentUpdate: function(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState);
    },

    login: function (e) {
        e.preventDefault();
        this.props.login({username: this.refs.username.value, password: this.refs.password.value});
    },

    render: function () {
        return (
            <div className="flexyfrog">
                <img src={require("../images/lefthoppis.png")}></img>
                <div className="loginForm">
                    <form className="form-signin">
                        <h2 className="form-signin-heading">Login</h2>
                        <label htmlFor="inputEmail" className="sr-only">Email address</label>
                        <input ref="username" className="form-control" placeholder="Username" required autoFocus />
                        <label htmlFor="inputPassword" className="sr-only">Password</label>
                        <input type="password" ref="password" className="form-control" placeholder="Password" required />
                        <button className="btn btn-lg btn-primary btn-block" onClick={this.login}>Send</button>
                    </form>
                </div> 
                <img src={require("../images/hoppis.png")}></img> 
            </div>
        );        
    }
});

function mapStateToProps(state) {
    return {      
    };
}

export default connect(mapStateToProps, authActions, null, { pure: false })(Login);