import React from 'react';
import {connect} from 'react-redux';

import GameView from './GameView.jsx';
import Login from './Login.jsx';
import Lobby from './Lobby.jsx';
import Menu from './Menu.jsx';
import shallowCompare from 'react-addons-shallow-compare';

const MainView = React.createClass({
    shouldComponentUpdate: function(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState);
    },

    getContentView: function () {
        if (this.props.tab.get('name') === 'lobby') {
            return <Lobby />;
        } else if (this.props.tab.get('name') === 'game') {
            return <GameView />;
        } else {
            return null;
        }
    },

    render: function () {
        if (this.props.loginToken) {
            return (
                <div>
                    <Menu />
                    {this.getContentView()}
                </div>
            );
        } else {
            return <Login />
        }
        
    }
});

function mapStateToProps(state) {
    return {
        loginToken: state.getIn(['authState', 'loginToken']),
        tab: state.getIn(['lobbyState', 'tab']),
        gameState: state.getIn(['gameState']),
    };
}

export default connect(mapStateToProps)(MainView);