import React from 'react';
import {connect} from 'react-redux';
import * as Konva from 'react-konva';
import Tiles from './Tiles.jsx';
import Units from './Units.jsx';
import Recruit from './Recruit.jsx';
import BuildCity from './BuildCity.jsx';
import SolveConflicts from './SolveConflicts.jsx';
import InformationBox from './InformationBox.jsx';
import {WIDTH, HEIGHT} from './util/canvas_util.js';
import shallowCompare from 'react-addons-shallow-compare';

const GameView = React.createClass({
    shouldComponentUpdate: function(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState);
    },

    getStage: function () {
        return this.refs.stage.getStage();
    },
    getTilesLayer: function () {
        return this.refs.tilesLayer;
    },
    isSolveConflictPhase: function () {
        return this.props.phase === this.props.phases.get('SOLVE_CONFLICTS_1') || this.props.phase === this.props.phases.get('SOLVE_CONFLICTS_2');
    },
    render: function () {
        var tiles = null;
        const isCurrentPlayer = (!this.isSolveConflictPhase() && this.props.username === this.props.players.get(this.props.currentPlayer).get('name')) ||
            (this.isSolveConflictPhase() && this.props.players.get(this.props.solvis) && this.props.username === this.props.players.get(this.props.solvis).get('name'));
        return (
            <div className="game-view">
                <Konva.Stage  ref="stage" width={WIDTH} height={HEIGHT}>                
                    <Konva.Layer ref="tilesLayer">
                        <Tiles  />
                    </Konva.Layer>
                    <Konva.Layer>
                        <Units getTilesLayer={this.getTilesLayer} getStage={this.getStage} />
                        <InformationBox />
                        {isCurrentPlayer && this.props.phases && this.props.phase === this.props.phases.get('RECRUIT') ? <Recruit getTilesLayer={this.getTilesLayer} getStage={this.getStage}/> : null}
                        {isCurrentPlayer && this.props.phases && this.props.phase === this.props.phases.get('MOVE_SETTLERS') ? <BuildCity getTilesLayer={this.getTilesLayer} getStage={this.getStage}/> : null}
                        {isCurrentPlayer && this.props.phases && this.isSolveConflictPhase() ? <SolveConflicts getTilesLayer={this.getTilesLayer} getStage={this.getStage}/> : null}
                    </Konva.Layer>
                </Konva.Stage>
            </div>
        );
    }
});

function mapStateToProps(state) {
    const currentGameId = state.getIn(['lobbyState', 'tab', 'data']).toString();
    return {
        phase: state.getIn(['gameState', currentGameId, 'phase']),
        phases: state.getIn(['gameState', currentGameId, 'phases']),
        players: state.getIn(['gameState', currentGameId, 'players']),
        currentPlayer: state.getIn(['gameState', currentGameId, 'currentPlayer']),
        solvis: state.getIn(['gameState', currentGameId, 'solvis']),
        tab: state.getIn(['lobbyState', 'tab']),
        currentGameId: currentGameId,
        username: state.getIn(['authState', 'username'])
    };
}

export default connect(mapStateToProps)(GameView);