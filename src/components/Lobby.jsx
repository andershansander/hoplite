import React from 'react';
import shallowCompare from 'react-addons-shallow-compare';
import {connect} from 'react-redux';

import * as lobbyActions from '../actions/lobby_actions.js';

const MainView = React.createClass({
    shouldComponentUpdate: function(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState);
    },

    getPendingGames: function () {
        return this.props.pendingGames.map(game => {
            const players = game.get('players').reduce((names, name) => {return names ? names + " " + name: name});
            return ( 
            <tr className="pendingGame" key={game.get('id')}>
                <td><button onClick={this.joinGame.bind(this, game.get('id'))}>Join</button></td>
                <td>{players}</td>
                <td>{game.get('maxPlayers')}</td>
            </tr>
            );
        });
    },

    joinGame: function (gameId) {
        this.props.joinGame({
            gameId: gameId
        })
    },
    createGame: function () {
        this.props.createGame({
            maxPlayers: 2
        })
    },
    render: function () {
        return (
            <div>
                <table className="lobbyGameList">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Players</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.getPendingGames()}
                    </tbody>
                </table>
                <button onClick={this.createGame}>Create game</button>
            </div>
        );
    }
});

function mapStateToProps(state) {
    return {
        pendingGames: state.getIn(['lobbyState', 'pendingGames'])
    };
}

export default connect(mapStateToProps, lobbyActions, null, { pure: false })(MainView);