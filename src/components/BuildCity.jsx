import React from 'react';
import shallowCompare from 'react-addons-shallow-compare';
import * as actionCreators from '../actions/unit_actions.js';
import {connect} from 'react-redux';
import * as Konva from 'react-konva';
import {MAP_WIDTH, MAP_RIGHT_MARGIN, INFORMATION_BOX_HEIGHT} from './util/canvas_util.js';

const Recruit = React.createClass({
    shouldComponentUpdate: function(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState);
    },

    imageExist: function (playerIndex, unit) {
        return this.props.images && this.props.images.get(playerIndex) && this.props.images.get(playerIndex).get(unit);
    },

    buildCity: function () {
        var pos = this.props.getStage().getPointerPosition();
        var shape = this.props.getTilesLayer().getIntersection(pos);
        if (!shape) {
            return;
        }
        const settlers = this.props.players.get(this.props.currentPlayer).get('settlers');
        let settlerIndex = settlers.findIndex(settler => {
            return settler.get('x') === shape.attrs.data.x && settler.get('y') === shape.attrs.data.y && !settler.get('hasMoved');
        });
        if (settlerIndex >= 0) {
            this.props.buildCity({settlerIndex: settlerIndex, gameId: this.props.currentGameId});
        }
    },

    getBuildButton: function () {
        if (!this.imageExist(this.props.currentPlayer, 'settler')) {
            return null;
        }
        const player = this.props.players.get(this.props.currentPlayer);
        if (player.get('food') < player.get('cities').size || !player.get('settlers').filter(settler => !settler.get('hasMoved')).size) {
            return <Konva.Circle x={MAP_WIDTH + MAP_RIGHT_MARGIN} y={INFORMATION_BOX_HEIGHT + 100} draggable={false} opacity={0.5} radius={40} fillPatternOffset={{x: -160, y: -150}} fillPatternScale={{x: 0.35, y: 0.35}} fillPatternImage={this.props.images.get(this.props.currentPlayer).get('settler')} />
        }        
        return <Konva.Circle opacity={1} onDragEnd={this.buildCity} draggable={true} x={MAP_WIDTH + 50} y={INFORMATION_BOX_HEIGHT + 100} radius={40} fillPatternOffset={{x: -160, y: -150}} fillPatternScale={{x: 0.35, y: 0.35}} fillPatternImage={this.props.images.get(this.props.currentPlayer).get('settler')} />
    },

    render: function () {
        return <Konva.Group>
            <Konva.Text x={MAP_WIDTH + MAP_RIGHT_MARGIN} y={INFORMATION_BOX_HEIGHT + 16} text="Build city" fontSize={30} fill="white" />
            {this.getBuildButton()}
        </Konva.Group>
    }
});

function mapStateToProps(state) {
    const currentGameId = state.getIn(['lobbyState', 'tab', 'data']).toString();
    return {
        images: state.getIn(['imageState', 'images']),
        players: state.getIn(['gameState', currentGameId, 'players']),
        currentPlayer: state.getIn(['gameState', currentGameId, 'currentPlayer']),
        currentGameId: currentGameId
    };
}

export default connect(mapStateToProps, actionCreators, null, { pure: false })(Recruit);