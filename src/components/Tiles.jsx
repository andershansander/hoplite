import React from 'react';
import shallowCompare from 'react-addons-shallow-compare';
import {connect} from 'react-redux';
import * as Konva from 'react-konva';
import {toCanvasCoodinates, HEX_RADIUS} from './util/canvas_util.js';

let tileKey = 0;

const Tiles = React.createClass({
    shouldComponentUpdate: function(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState);
    },

    imageExist: function (tileTypeKey) {
        return this.props.images && this.props.images.get('tile') && this.props.images.get('tile').get(tileTypeKey);
    },

    generateTile({x, y, rowIndex, columnIndex, tileType}) {
        if (tileType === 'S') {
            tileType = "V";
        }
        return this.imageExist(tileType) ? 
            <Konva.RegularPolygon data={{x: columnIndex, y: rowIndex}} key={tileKey++} x={x} y={y} sides={6} radius={HEX_RADIUS} fillPatternScale={{x: 0.15, y: 0.15}} fillPatternOffset={{x : 338, y : 300}} fillPatternImage={this.props.images.get('tile').get(tileType)} />
             : null;
    },

    render: function () {
        let tiles = [];
        if (this.props.map) {
            this.props.map.forEach(
                (row, rowIndex) => {
                    row.forEach((tileType, columnIndex) => {
                        const options = Object.assign({ columnIndex, rowIndex, tileType }, toCanvasCoodinates({ x: columnIndex, y: rowIndex }));
                        tiles.push(this.generateTile(options));
                    });
                });
        }
        return <Konva.Group>{tiles}</Konva.Group>;
    }
});

function mapStateToProps(state) {
    const currentGameId = state.getIn(['lobbyState', 'tab', 'data']).toString();
    return {
        map: state.getIn(['gameState', currentGameId, 'map']),
        images: state.getIn(['imageState', 'images']),
        currentGameId: currentGameId
    };
}

export default connect(mapStateToProps)(Tiles);