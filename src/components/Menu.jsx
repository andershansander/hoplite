import React from 'react';
import shallowCompare from 'react-addons-shallow-compare';
import {connect} from 'react-redux';

import * as lobbyActions from '../actions/lobby_actions.js';

const Menu = React.createClass({
    shouldComponentUpdate: function(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState);
    },

    getActiveGameTabs: function () {
        return this.props.activeGames.map(activeGame => {
            const id = activeGame.get('id');
            const className = "menuItem " + (this.props.tab.get('name') === 'game' && this.props.tab.get('data') === id ? 'active' : '');
            return <button key={id} onClick={this.props.showGameTab.bind(null, {gameId: id})} className={className}>Game {id}</button>;
        });
    },

    render: function () {
        const lobbyTabClassName = "menuItem " + (this.props.tab.get('name') === 'lobby' ? 'active' : '');
        return (
                <div className="menu">
                    <div className="menuMainBackground" />
                    <div className="menuBottomBackground" />
                    <button onClick={this.props.showLobbyTab} className={lobbyTabClassName}>Lobby</button>
                    {this.getActiveGameTabs()}
                </div>
        );
    }
});

function mapStateToProps(state) {
    return {
        tab: state.getIn(['lobbyState', 'tab']),
        activeGames: state.getIn(['lobbyState', 'activeGames'])
    };
}

export default connect(mapStateToProps, lobbyActions, null, { pure: false })(Menu);