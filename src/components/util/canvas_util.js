export const LEFT_MARGIN = 50;
export const TOP_MARGIN = 55;
export const HEX_RADIUS = 50;
export const WIDTH = 1000;
export const HEIGHT = 600;
export const MAP_WIDTH = 600;
export const MAP_RIGHT_MARGIN = 50;
export const INFORMATION_BOX_HEIGHT = 180;

export function toCanvasCoodinates({x, y}) {
    var rowXOffset = ((y+1) % 2) * HEX_RADIUS * 7/8;
    return {
        x: LEFT_MARGIN + rowXOffset + x * HEX_RADIUS * 14/8,
        y: TOP_MARGIN + y * HEX_RADIUS * 1.5
    };
}
