import React from 'react';
import {connect} from 'react-redux';
import * as Konva from 'react-konva';
import {toCanvasCoodinates, MAP_WIDTH, MAP_RIGHT_MARGIN, INFORMATION_BOX_HEIGHT} from './util/canvas_util.js';
import * as actionCreators from '../actions/misc_game_actions.js';
import {List} from 'immutable';
import shallowCompare from 'react-addons-shallow-compare';

let removalMarkerKeyCounter = 1;

const Map = React.createClass({
    shouldComponentUpdate: function(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState);
    },
    render: function () {
        const player = this.props.players.get(this.props.solvis);
        const removeMarkers = !player.get('hoplitesToRemove') ? null : player.get('hoplitesToRemove').map(hopliteToRemove => {
            const removeLocation = hopliteToRemove.get('location');
            const markedForRemovalAtLocation = player.get('hoplites').filter(hoplite => hoplite.get('x') === removeLocation.get('x') && hoplite.get('y') === removeLocation.get('y') && hoplite.get('markedForRemoval')).size;
            const remainingToRemove = hopliteToRemove.get('amount') - markedForRemovalAtLocation;
            const location = toCanvasCoodinates({x: removeLocation.get('x'), y: removeLocation.get('y')});
            return <Konva.Text key={removalMarkerKeyCounter++} x={location.x - 14} y={location.y - 45} text={remainingToRemove} fontSize={48} fill="red" />  
        });
        return <Konva.Group>
            {removeMarkers}
        </Konva.Group>
    }
});

function mapStateToProps(state) {
    const currentGameId = state.getIn(['lobbyState', 'tab', 'data']).toString();
    return {
        players: state.getIn(['gameState', currentGameId, 'players']),
        solvis: state.getIn(['gameState', currentGameId, 'solvis']),
        currentGameId: currentGameId
    };
}

export default connect(mapStateToProps, actionCreators, null, { pure: false })(Map);