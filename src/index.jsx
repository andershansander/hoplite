import React from 'react';
import ReactDOM from 'react-dom';
import Main from './components/Main.jsx';
import store from './store.js';
import {Provider} from 'react-redux';
import {PLAYER_UNIT_IMAGES, CARD_IMAGES, TILE_IMAGES} from './images/images.js';
import {loadUnitImage, loadCardImage, loadTileImage} from './actions/image_actions.js';

for (let playerIndex = 0; playerIndex < PLAYER_UNIT_IMAGES.length; playerIndex++) {
  Object.keys(PLAYER_UNIT_IMAGES[playerIndex]).forEach(unitKey => {
      store.dispatch(loadUnitImage({
        playerIndex: playerIndex,
        unit: unitKey
      }));
    })
}

Object.keys(CARD_IMAGES).forEach(cardId => {
  store.dispatch(loadCardImage({
      cardId: cardId
    }));
});

Object.keys(TILE_IMAGES).forEach(tileTypeKey => {
  store.dispatch(loadTileImage({
      tileTypeKey: tileTypeKey
    }));
});

require("./css/bootstrap.css");
require("./css/hoplite.css");

ReactDOM.render(
  <Provider store={store}>
    <Main />
  </Provider>,
  document.getElementById('app')
); 