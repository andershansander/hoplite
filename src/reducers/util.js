import store from '../store.js';

export function dispatch (action) {
  setTimeout(() => {
    store.dispatch(action);
  }, 0);
}