import {Map, fromJS} from 'immutable';
import * as lobbyActions from '../actions/lobby_actions.js';
import * as gameActions from '../actions/misc_game_actions.js';
import {dispatch} from './util.js';

function onSetState(action) {
    dispatch(lobbyActions.setState(fromJS({
        pendingGames: action.state.pendingGames,
        activeGames: action.state.activeGames
    })));
    if (action.state.gameData) {
        Object.keys(action.state.gameData).forEach(gameId => {
            dispatch(gameActions.setState(
                fromJS(action.state.gameData[gameId]),
                {gameId}
            ));
        });
    }    
}

const reducer = function(state = new Map(), action) {
  console.debug("Got action from server", action);
  let nextState = state;
  switch (action.type) {
  case 'set_state':
    onSetState(action);
    break;
  default:
    console.debug("Unhandled server action " + action.type);      
    break;
  }
  return state;
};

export default reducer;