import {Map} from 'immutable';
import {PLAYER_UNIT_IMAGES, CARD_IMAGES, TILE_IMAGES} from '../images/images.js';
import * as imageActions from '../actions/image_actions.js';
import {dispatch} from './util.js';

function onUnitImageLoaded(state, {image, playerIndex, unit}) {
  return state.setIn(['images', playerIndex, unit], image);
}

function onCardImageLoaded(state, {image, cardId}) {
  return state.setIn(['images', "cards", cardId], image);
}

function onTileImageLoaded(state, {image, tileTypeKey}) {
  return state.setIn(['images', "tile", tileTypeKey], image);
}

function onLoadUnitImage(state, {playerIndex, unit}) {
  const image = new Image();
  image.onload = () => {
    dispatch(imageActions.unitImageLoaded({playerIndex, unit, image}));
  };
  image.src = PLAYER_UNIT_IMAGES[playerIndex][unit];
  return state;
}

function onLoadTileImage(state, {tileTypeKey}) {
  const image = new Image();
  image.onload = () => {
    dispatch(imageActions.tileImageLoaded({tileTypeKey, image}));
  };
  image.src = TILE_IMAGES[tileTypeKey];
  return state;
}

function onLoadCardImage(state, {cardId}) {
  const image = new Image();
  image.onload = () => {
    dispatch(imageActions.cardImageLoaded({cardId, image}));
  };
  image.src = CARD_IMAGES[cardId];
  return state;
}


const reducer = function(state = new Map(), action) {
  console.debug("Performing image action", action);
  switch (action.type) {
  case imageActions.types.LOAD_UNIT_IMAGE:
    return onLoadUnitImage(state, {playerIndex: action.playerIndex, unit: action.unit});
  case imageActions.types.LOAD_CARD_IMAGE:
    return onLoadCardImage(state, {cardId: action.cardId});
  case imageActions.types.LOAD_TILE_IMAGE:
    return onLoadTileImage(state, {tileTypeKey: action.tileTypeKey});
  case imageActions.types.UNIT_IMAGE_LOADED:
    return onUnitImageLoaded(state, {image: action.image, playerIndex: action.playerIndex, unit: action.unit});
  case imageActions.types.CARD_IMAGE_LOADED:
    return onCardImageLoaded(state, {image: action.image, cardId: action.cardId});
  case imageActions.types.TILE_IMAGE_LOADED:
    return onTileImageLoaded(state, {image: action.image, tileTypeKey: action.tileTypeKey});  
  default:
    console.debug("Unhandled image action", action);
  }
  return state;
};

export default reducer;