import {Map, fromJS} from 'immutable';
import imageReducer from './image_loading_reducer.js';
import gamesReducer from './games_reducer.js';
import authReducer from './auth_reducer.js';
import lobbyReducer from './lobby_reducer.js';
import serverReducer from './server_action_reducer.js';

const INITIAL_STATE = fromJS({
  lobbyState: {
    tab: {
      name: "lobby"
    },
    pendingGames: [

    ],
    activeGames: [

    ]
  }
});

const reducer = function(state = INITIAL_STATE, action) {
  if (action.remote === true) {
    console.debug("Sending remote action ", action);
    const socket = state.getIn(['authState', 'socket']);
    if (socket) {
      socket.emit('action', action);
    } else {
      console.error("There is no socket to send the remote action over.");
    }
    return state;
  }

  switch (action.category) {
  case 'image':
    return state.set('imageState', imageReducer(state.get('imageState'), action));
  case 'auth':
    return state.set('authState', authReducer(state.get('authState'), action));
  case 'game':
    return state.set('gameState', gamesReducer(state.get('gameState'), action));
  case 'lobby':
    return state.set('lobbyState', lobbyReducer(state.get('lobbyState'), action));
  case 'server': {
    return serverReducer(state, action);
  }

  default:
    console.debug("Unhandled action category: " + action.category); 
  }
  return state;
};

export default reducer;