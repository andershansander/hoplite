import {Map, List} from 'immutable';
import * as unitActions from '../actions/unit_actions.js';
import * as miscActions from '../actions/misc_game_actions.js';
import {dispatch} from './util.js';

let stateRefreshCounter = 0;

function markHopliteForRemoval(state, {hopliteIndex}) {
  const hoplite = state.getIn(['players', state.get('solvis'), 'hoplites', hopliteIndex]);
  return state.setIn(['players', state.get('solvis'), 'hoplites', hopliteIndex], hoplite.set('markedForRemoval', "yeeeees!"));
}

function confirmHopliteRemoval(state) {
  const hoplitesToRemove = [];
  state.getIn(['players', state.get('solvis'), 'hoplites']).forEach((hoplite, hopliteIndex) => {
    if (hoplite.get('markedForRemoval')) {
      hoplitesToRemove.push(hopliteIndex);
    }
  });
  dispatch(unitActions.removeHoplites({
    hopliteIndices: hoplitesToRemove,
    gameId: state.get('id')
  }));

  return state;
}


function isInSolveConflictState(state) {
  return state.get('phase') === state.get('phases').get('SOLVE_CONFLICTS_1') || state.get('phase') === state.get('phases').get('SOLVE_CONFLICTS_2'); 
}

function setState(state, nextState) {
  return state.merge(nextState);
}

const reducer = function(state = new Map(), action) {
  console.debug("Performing game action", action);
  switch (action.type) {

  case miscActions.types.SET_GAME_STATE:
    return (setState(state, action.state));  
  case unitActions.types.MARK_FOR_REMOVAL:
    return markHopliteForRemoval(state, {hopliteIndex: action.hopliteIndex});
  case unitActions.types.CONFIRM_HOPLITE_REMOVAL:
    return confirmHopliteRemoval(state);
  case miscActions.types.REFRESH:
    return state.set('refreshId', stateRefreshCounter++);
  default:
    if (!action.meta || !action.meta.remote) {
      console.debug("Unhandled game action " + action.type);  
    } else {
      dispatch(Object.assign({remote: true}, action));
    }
    break;
  }
  return state;
};

export default reducer;