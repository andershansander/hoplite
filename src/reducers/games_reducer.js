import gameReducer from './game_reducer.js';

const reducer = function(state = new Map(), action) {
  return state.set(action.gameId, gameReducer(state.get(action.gameId), action));
};

export default reducer;