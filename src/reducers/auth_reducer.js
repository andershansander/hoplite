import {Map} from 'immutable';
import * as actions from '../actions/auth_actions.js';
import {dispatch} from './util.js';
import {setState} from '../actions/server_actions.js';
import io from 'socket.io-client';
import {wsServer, httpServer} from '../config.js';

function cacheLogin(loginData) {
  localStorage.setItem('login', JSON.stringify(loginData));
}

function cleanLoginState(state) {
  return state.remove('loginFailed').remove('pendingLogin').remove('loginToken').remove('validUntil').remove('username');
}

function setLoginState(state, {token, validUntil, username}) {
  let nextState = cleanLoginState(state);
  return nextState.set('loginToken', token).set('validUntil', validUntil).set('username', username);
}

function onLoginSucceeded(state, loginData) {
  cacheLogin(loginData);
  const nextState = setLoginState(state, loginData);
  setupWebsocket(nextState);
  return nextState;
}

function onLoginFailed(state, {message}) {
  return cleanLoginState(state).set('loginFailed', message);
}

function onSocketConnected(state, socket) {
  return state.set('socket', socket);
}

function onSocketDisconnected(state) {
  return state.remove('socket');
}

function setupWebsocket(state) {
  console.debug("Connecting websocket to " + wsServer);
  const socket = io(wsServer);

  socket.on('disconnected', (reason) => {
    dispatch(actions.socketDisconnected(reason));
  });

  socket.on('state', state => {
      console.debug("Got new state from server ", state);
      dispatch(setState(state));
    }
  );

  socket.on('connection_validated', () => {
    console.debug("Connection validated!");
    dispatch(actions.socketConnected(socket));
  });

  socket.on('connect', () => {
    console.debug("Logging in with token " + state.get('loginToken') + " and username " + state.get('username'));
    socket.emit('login', {
      token: state.get('loginToken'),
      username: state.get('username')
    });
  });  
}

function login(state, {username, password}) {
  var http = new XMLHttpRequest();
  http.open("POST", httpServer + "login?username=" + username + "&password=" + password, true);
  http.setRequestHeader('Accept', 'application/json');
  http.onreadystatechange = function() {//Call a function when the state changes.
      if(http.readyState === 4) {
        if (http.status !== 200) {
          dispatch(actions.loginFailed({message: http.status}));
        } else {
          dispatch(actions.loginSucceeded(JSON.parse(http.responseText)));
        }          
      }
  }
  http.send();
  return cleanLoginState(state).set('pendingLogin', true);
}

function autoLogin(state) {
  const savedLogin = localStorage.getItem('login');
  if (savedLogin) {
    const loginState = JSON.parse(savedLogin);
    if (loginState.validUntil && loginState.validUntil < Date.now() - 60000) {
      return setLoginState(state, loginState);
    }    
  }
  return state;
}

const reducer = function(state = new Map(), action) {
  console.debug("Performing auth action", action);
  switch (action.type) {
  case actions.types.LOGIN:
    return login(state, {username: action.username, password: action.password});
  case actions.types.LOGIN_SUCCEEDED:
    return onLoginSucceeded(state, {token: action.token, validUntil: action.validUntil, username: action.username});
  case actions.types.LOGIN_FAILED:
    return onLoginFailed(state, {token: action.message});
  case actions.types.TRY_AUTO_LOGIN:
    return autoLogin(state);
  case actions.types.SOCKET_CONNECTED:
    return onSocketConnected(state, action.socket);
  case actions.types.SOCKET_DISCONNECTED:
    return onSocketDisconnected(state);
  default:
    console.debug("Unhandled auth action", action);
  }
};

export default reducer;