import {Map, List, fromJS} from 'immutable';
import * as actions from '../actions/lobby_actions.js';
import {dispatch} from './util.js';

function onTabChanged(state, {tab}) {
    return state.set('tab', Map(tab));
}

const reducer = function(state = new Map(), action) {
  console.debug("Performing lobby action", action);
  switch (action.type) {    
  case actions.types.SET_LOBBY_STATE:
    return state.merge(action.state);
  case actions.types.SHOW_LOBBY_TAB:
    return onTabChanged(state, {tab: {name: 'lobby'}});
  case actions.types.SHOW_GAME_TAB:
    return onTabChanged(state, {tab: {name: 'game', data: action.gameId}});
  case actions.types.JOIN_GAME:
  case actions.types.CREATE_GAME:
    dispatch(Object.assign({remote: true}, action));
    return state;
  default:
    console.debug("Unhandled lobby action", action);
  }
};

export default reducer;